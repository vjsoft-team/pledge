<?php
include_once 'core/init.php';

$link = new Link();
if (!$link->isloggedIn()) {
  Redirect::to('login.php');
 }
  $page_id = "dashboard";
?>
















<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <title>Pawn - PLEDGE</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

  <link rel="apple-touch-icon" href="pages/ico/60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
  <link rel="icon" type="image/x-icon" href="favicon.ico" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
  <link href="assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
  <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
  <!--[if lte IE 9]>
	<link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
  <script type="text/javascript">
    /* <![CDATA[ */
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-56895490-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();

    /* ]]> */
  </script>
</head>

<body class="fixed-header dashboard">

  <?php include_once 'navbar.php'; ?>

  <div class="page-container">

    <?php include_once 'header.php'; ?>

    <div class="page-content-wrapper">

      <div class="content">

        <div class="container-fluid container-fixed-lg bg-white m-t-50">
    <div class="row">
        <div class="col-sm-5">
          <?php
          if ($_GET['status'] === success) {
            $saCode = '16d390';
            $aadhaarIdJs = $_SESSION['aadhaar'];
            $requestId = $_GET['requestId'];
            $uuid = $_GET['uuid'];
            $salt = 'c8b6a27e69';
            $aadhaarId = $aadhaarIdJs;
            $hash_seq = $uuid . '|' . $saCode . '|' . $aadhaarId . '|' . $requestId . '|' . $salt;
            $hash = hash( 'sha256', $hash_seq );
            $data = [
              "saCode" => $saCode,
              "uuid" => $uuid,
              "requestId" => $requestId,
              "aadhaarId" => $aadhaarId,
              "hash" => $hash
            ];
            $data_string = json_encode($data);

            $ch = curl_init('https://preprod.aadhaarbridge.com/kua/_kyc');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
              'Content-Type: application/json',
              'Content-Length: ' . strlen($data_string))
            );

            $result = curl_exec($ch);

            // print_r($result);
            // echo "<br>";
            $obj = json_decode($result, true);
            // print_r($obj);
            // echo "<br>";
            if ($obj['kyc']['success'] === 1) {
              $_SESSION['aadhaar_data'] = $obj;
              Redirect::to('biometric_success.php');
            }


            // $json =  json_decode($result);

            // print_r($json->kyc);
          }

          ?>

            <div class="panel panel-transparent">
                <div class="panel-heading">
                  <?php // print_r($_SESSION['aadhaar_data']); ?>
                    <div class="panel-title">Customer Information
                    </div>
                </div>
                <div class="panel-body">
                  <div>
                      <div class="profile-img-wrapper inline m-t-10" style="height: 55px; width: 55px;">
                        <?php if ($_SESSION['aadhaar_data']['kyc']['photo']): ?>
                          <img width="55" height="55" src="data:image/png;base64, <?= $_SESSION['aadhaar_data']['kyc']['photo'] ?>" alt="" data-src="data:image/png;base64, <?= $_SESSION['aadhaar_data']['kyc']['photo'] ?>" data-src-retina="data:image/png;base64, <?= $_SESSION['aadhaar_data']['kyc']['photo']; ?>">

                        <?php endif; ?>
                          <div class="chat-status available">
                          </div>
                      </div>
                      <div class="inline m-l-10">
                          <h5 class=""><?= $_SESSION['aadhaar_data']['kyc']['poi']['name']; ?> (<?= $_SESSION['aadhaar_data']['kyc']['poi']['gender']; ?>)
                              <br> <?= $_SESSION['aadhaar_data']['kyc']['poi']['dob']; ?></h5>
                      </div>
                  </div>
                    <h3><?= $_SESSION['aadhaar_data']['kyc']['poa']['co'] ?>
                      <br>
                      Address: <br>
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      &nbsp;
                      &nbsp;
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['house']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['street']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['lc']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['dist']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['state']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['country']; ?>,
                       <?= $_SESSION['aadhaar_data']['kyc']['poa']['pc']; ?>
                    </h3>
                    <h1>Aadhaar Id: <?= $_SESSION['aadhaar_data']['aadhaar-id'] ?></h1>
                    <br>

                </div>
            </div>



        </div>
        <div class="col-sm-7">

            <div class="panel panel-transparent">
                <div class="panel-body">
                    <form id="otpForm" action="https://preprod.aadhaarbridge.com/kua/_init" role="form" autocomplete="off" method="post">
                      <p class="m-t-10">Address</p>
                      <div class="form-group-attached">

                          <div class="form-group form-group-default">
                              <label>Same as AADHAAR <i class="fa fa-map text-complete m-l-5"></i>
                              </label>
                              <!-- <label class="inline">Availability</label> -->
                              <span class="bg-transparent">
                                <input type="checkbox" name="address" id="address" data-init-plugin="switchery" data-size="big" data-color="primary" checked/>
                              </span>
                          </div>
                          <div class="row clearfix">
                              <div class="col-sm-6">
                                  <div class="form-group form-group-default required">
                                      <label>Door No</label>
                                      <input id="house" type="text" name="house" class="form-control" required value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['house']; ?>">
                                      <input id="house_aadhaar" type="hidden" name="house_aadhaar" required value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['house']; ?>">
                                  </div>
                              </div>
                              <div class="col-sm-6">
                                  <div class="form-group form-group-default">
                                      <label>Street</label>
                                      <input id="street" type="text" class="form-control" name="street" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['street']; ?>">
                                      <input id="street_aadhaar" type="hidden" name="street_aadhaar" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['street']; ?>">
                                  </div>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-sm-6">
                                  <div class="form-group form-group-default required">
                                      <label>City</label>
                                      <input type="text" id="city" class="form-control" name="city" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['lc']; ?>">
                                      <input type="hidden" id="city_aadhaar" name="city_aadhaar" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['lc']; ?>">
                                  </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group form-group-default required">
                                    <label>State</label>
                                    <input type="text" id="state" class="form-control" name="state" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['state']; ?>">
                                    <input type="hidden" id="state_aadhaar"  name="state_aadhaar" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['state']; ?>">
                                </div>
                              </div>
                              <div class="col-sm-3">
                                <div class="form-group form-group-default required">
                                    <label>Pincode</label>
                                    <input type="text" class="form-control" id="pc" name="pc" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['pc']; ?>">
                                    <input type="hidden" id="pc_aadhaar" name="pc" value="<?= $_SESSION['aadhaar_data']['kyc']['poa']['pc']; ?>">
                                </div>
                              </div>
                          </div>

                      </div>
                        <p class="m-t-15">Otp Validation</p>
                        <div class="form-group-attached">
                            <div class="form-group form-group-default required">
                                <label>Mobile No</label>
                                <span class="help">e.g. "(123) 456-7890"</span>
                                <input type="text" id="phone" name="phone" class="form-control">
                            </div>

                        </div>

                        <br>
                        <div class="pull-left">
                            <div class="checkbox check-success">
                                <input type="checkbox" name="accept" value="1" id="checkbox-agree" required>
                                <label for="checkbox-agree">I hereby certify that the information above is true and accurate
                                </label>
                            </div>
                        </div>
                        <br>
                        <?php require_once 'otp.php'; ?>
                        <input type="hidden" name="saCode" value="16d390" >
                        <input type="hidden" name="aadhaarId" id="aadhaarId" value="<?= '286004871682'; ?>">
                        <input type="hidden" name="requestId" id="requestId" value="<?= $requestId; ?>" >
                        <input type="hidden" name="purpose" value="TEsting by vJ" >
                        <input type="hidden" name="modality" value="otp" >
                        <input type="hidden" name="channel" value="BOTH" >
                        <input type="hidden" name="successUrl" value="http://192.168.1.112/pledge/biometric_success.php" >
                        <input type="hidden" name="failureUrl" value="http://192.168.1.112/aadhaar/failure.php"  >
                        <input type="hidden" name="hash" id="hash" value="<?= $hash; ?>" >
                        <button class="btn btn-success pull-right" type="submit">Submit</button>
                        <!-- <button class="btn btn-default"><i class="pg-close"></i> Clear</button> -->
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>



      </div>

      <?php include_once 'footer.php'; ?>

    </div>

  </div>

  <?php include_once 'search.php'; ?>

  <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
  <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
  <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-bez/jquery.bez.min.js"></script>
  <script src="assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-actual/jquery.actual.min.js"></script>
  <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
  <script type="text/javascript" src="assets/plugins/select2/js/select2.full.min.js"></script>
  <script type="text/javascript" src="assets/plugins/classie/classie.js"></script>
  <script src="assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
  <script src="assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <script type="text/javascript" src="assets/plugins/jquery-autonumeric/autoNumeric.js"></script>
  <script type="text/javascript" src="assets/plugins/dropzone/dropzone.min.js"></script>
  <script type="text/javascript" src="assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js"></script>
  <script type="text/javascript" src="assets/plugins/jquery-inputmask/jquery.inputmask.min.js"></script>
  <script src="assets/plugins/bootstrap-form-wizard/js/jquery.bootstrap.wizard.min.js" type="text/javascript"></script>
  <!-- <script src="assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script> -->
  <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="assets/plugins/summernote/js/summernote.min.js" type="text/javascript"></script>
  <script src="assets/plugins/moment/moment.min.js"></script>
  <script src="assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
  <script src="assets/plugins/bootstrap-typehead/typeahead.bundle.min.js"></script>
<script src="assets/plugins/bootstrap-typehead/typeahead.jquery.min.js"></script>
<script src="assets/plugins/handlebars/handlebars-v4.0.5.js"></script>

  <script src="pages/js/pages.min.js"></script>

  <script src="assets/js/form_elements.js" type="text/javascript"></script>

  <script src="assets/js/form_wizard.js" type="text/javascript"></script>
  <script src="assets/js/scripts.js" type="text/javascript"></script>

  <!-- <script src="assets/js/demo.js" type="text/javascript"></script> -->
  <script>
      window.intercomSettings = {
          app_id: "xt5z6ibr"
      };
  </script>
  <!-- <script type="text/javascript" src="hash.js"> </script> -->
  <script type="text/javascript" src="otp.js"> </script>
  <script>
    $(document).ready(function() {
      $("#address").change(function() {
        if(this.checked) {
          var house = $('#house_aadhaar').val();
          var street = $('#street_aadhaar').val();
          var city = $('#city_aadhaar').val();
          var state = $('#state_aadhaar').val();
          var pc = $('#pc_aadhaar').val();

          $('#house').val(house);
          $('#street').val(street);
          $('#city').val(city);
          $('#state').val(state);
          $('#pc').val(pc);
          localStorage.setItem('address', 1);
        } else {
          $('#house').val('');
          $('#street').val('');
          $('#city').val('');
          $('#state').val('');
          $('#pc').val('');
          localStorage.setItem('address', 0);
        }
      });
      $("#checkbox-agree").change(function() {
        if(this.checked) {
          localStorage.setItem('accept', 1);
        } else {
          localStorage.setItem('accept', 0);
        }
      });
        // $('#myForm').validate();
    });
  </script>


</body>

</html>
