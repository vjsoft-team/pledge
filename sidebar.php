<div class="sidebar-menu">

    <ul class="menu-items">
        <li class="m-t-30">
            <a href="index.php" class="detailed">
                <span class="title">Dashboard</span>
                <span class="details">Home Page</span>
            </a>
            <span class="<?php echo ($page_id == "dashboard") ? "bg-success" : ""; ?> icon-thumbnail"><i class="pg-home"></i></span>
        </li>
        <li class="">
            <a href="pawn.php" class="detailed">
                <span class="title">Pawn</span>
                <span class="details">New Pawn</span>
            </a>
            <span class="icon-thumbnail">P</span>
        </li>

        <li class="">
            <a href="#" target=""><span class="title">Reports</span></a>
            <span class="icon-thumbnail"><i class="pg-note"></i></span>
        </li>
        <li class="">
            <a href="ornament_types.php" target=""><span class="title">Ornament Types</span></a>
            <span class="icon-thumbnail"><i class="pg-note"></i></span>
        </li>

    </ul>
    <div class="clearfix"></div>
</div>
