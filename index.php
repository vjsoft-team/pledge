<?php
include_once 'core/init.php';

$link = new Link();
if (!$link->isloggedIn()) {
  Redirect::to('login.php');
 }
  $page_id = "dashboard";
?>
<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8" />
  <title>Pawn - PLEDGE</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />

  <link rel="apple-touch-icon" href="pages/ico/60.png">
  <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
  <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
  <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
  <link rel="icon" type="image/x-icon" href="favicon.ico" />
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta content="" name="description" />
  <meta content="" name="author" />
  <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/bootstrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
  <link href="assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
  <link href="assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
  <link href="pages/css/pages-icons.css" rel="stylesheet" type="text/css">
  <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
  <!--[if lte IE 9]>
	<link href="assets/plugins/codrops-dialogFx/dialog.ie.css" rel="stylesheet" type="text/css" media="screen" />
	<![endif]-->
  <script type="text/javascript">
    /* <![CDATA[ */
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-56895490-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script');
      ga.type = 'text/javascript';
      ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(ga, s);
    })();

    /* ]]> */
  </script>
</head>

<body class="fixed-header dashboard">

  <?php include_once 'navbar.php'; ?>

  <div class="page-container">

    <?php include_once 'header.php'; ?>

    <div class="page-content-wrapper">

      <div class="content sm-gutter">

        <div class="container-fluid padding-25 sm-padding-10">

          <div class="row">
            <div class="col-md-12 col-lg-12 col-xlg-12">
              <div class="row">
                <div class="col-md-3 m-b-10">

                  <a href="pawn.php">
                    <div class="widget-8 panel no-border bg-success no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">

                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top relative">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="p-l-20">
                                  <h3 class="no-margin p-b-5 text-white">Pawn</h3>

                                </div>
                              </div>

                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </a>

                </div>
                <div class="col-md-3 m-b-10">

                  <a href="#">
                    <div class="widget-8 panel no-border bg-primary no-margin widget-loader-bar">
                      <div class="container-xs-height full-height">
                        <div class="row-xs-height">

                        </div>
                        <div class="row-xs-height">
                          <div class="col-xs-height col-top relative">
                            <div class="row">
                              <div class="col-sm-6">
                                <div class="p-l-20">
                                  <h3 class="no-margin p-b-5 text-white">Report</h3>

                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </a>

                </div>

                <!-- <div class="col-md-3">

                  <div class="widget-10 panel no-border bg-white no-margin widget-loader-bar">
                    <div class="panel-heading top-left top-right">
                      <div class="panel-title text-black hint-text">
                        <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>
    </span>
                      </div>
                      <div class="panel-controls">
                        <ul>
                          <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="panel-body p-t-40">
                      <div class="row">
                        <div class="col-sm-12">
                          <h4 class="no-margin p-b-5 text-danger semi-bold">APPL 2.032</h4>
                          <div class="pull-left small">
                            <span>WMHC</span>
                            <span class="text-success font-montserrat">
    <i class="fa fa-caret-up m-l-10"></i> 9%
    </span>
                          </div>
                          <div class="pull-left m-l-20 small">
                            <span>HCRS</span>
                            <span class="text-danger font-montserrat">
    <i class="fa fa-caret-up m-l-10"></i> 21%
    </span>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="p-t-10 full-width">
                        <a href="#" class="btn-circle-arrow b-grey"><i class="pg-arrow_minimize text-danger"></i></a>
                        <span class="hint-text small">Show more</span>
                      </div>
                    </div>
                  </div>

                </div> -->
              </div>
            </div>



          </div>


        </div>

      </div>

      <?php include_once 'footer.php'; ?>

    </div>

  </div>

  <?php include_once 'search.php'; ?>

  <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
  <script src="assets/plugins/modernizr.custom.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
  <script src="assets/plugins/bootstrapv3/js/bootstrap.min.js" type="text/javascript"></script>
  <script src="assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>


  <script src="pages/js/pages.min.js"></script>
  <script src="assets/js/scripts.js" type="text/javascript"></script>

</body>

</html>
