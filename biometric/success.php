<?php

  // echo "Hash: ". $_GET['hash'];
  // echo "</br>";
  // echo "Uuid: ". $_GET['uuid'];
  // echo "</br>";
  // echo "Request Id: ". $_GET['requestId'];
  // echo "</br>";
  // echo "Status: ". $_GET['status'];

  $saCode = '16d390';
  $aadhaarId = '515383281069';
  $requestId = $_GET['requestId'];
  $uuid = $_GET['uuid'];
  $salt = 'c8b6a27e69';
  $hash_seq = $uuid . '|' . $saCode . '|' . $aadhaarId . '|' . $requestId . '|' . $salt;
  $hash = hash( 'sha256', $hash_seq );

  $data = [
    "saCode" => $saCode,
    "uuid" => $uuid,
    "requestId" => $requestId,
    "aadhaarId" => $aadhaarId,
    "hash" => $hash
  ];
  $data_string = json_encode($data);

  $ch = curl_init('https://preprod.aadhaarbridge.com/kua/_kyc');
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($data_string))
  );

  $result = curl_exec($ch);

  // print_r($result[0]->success);

  $obj = json_decode($result, true);
  // echo "<pre>"; print_r($obj['kyc']);

  // $json =  json_decode($result);

  // print_r($json->kyc);
?>


<img src="data:image/png;base64, <?= $obj['kyc']['photo']; ?>" alt="">
