<?php

  error_reporting(0);
  $saCode = '16d390';
  $aadhaarId = '515383281069';
  $requestId = rand(11111111, 999999999);
  $salt = 'c8b6a27e69';
  $hash_seq = $saCode . '|' . $aadhaarId . '|' . $requestId . '|' . $salt;
  $hash = hash( 'sha256', $hash_seq );


  $json = [];

  $json = [
    'consent'=> 'Y',
    'modality' => [
      'demographics' => false,
      'fp-image' => false,
      'fp-minutae' => false,
      'iris' => false,
      'otp' => true,
      'pin' => false,
    ],
    'pid' => [
      'type' => $_POST['pid_type'],
      'value' => $_POST['pid_value'],
    ],
    'aadhaar-id' => $aadhaarId,
    'hmac' => $_POST['Hmac'],
    'session-key' => [
      'cert-id' => $_POST['cert_id'],
      'value' => $_POST['cert_value'],
    ],
    'unique-device-code' => $_POST['unique_device_code'],
    'dpId' => $_POST['dpId'],
    'rdsId' => $_POST['rdsId'],
    'rdsVer' => $_POST['rdsVer'],
    'dc' => $_POST['dc'],
    'mi' => $_POST['mi'],
    'mc' => $_POST['mc']
  ];

  // header('Content-Type: application/json');
  $result = json_encode($json);

  $ver = '2.1';
  $ra = "F";
  $rc = "Y";
  $lr = "N";
  $de = "N";
  $pfr = "N";

  echo $wadh_seq = $ver.$ra.$rc.$lr.$de.$pfr;
  $wadh = hash( 'sha256', $wadh_seq );
  echo $wadh;

?>
<html>
<head>
    <title>RD Service Client </title>
    <style type="text/css">
        body
        {
            font-family: 'Segoe UI';
            background-color: #DDDDDD;
            margin: 0px 5px 5px 5px;
            padding: 0px 5px 5px 5px;
            color: #555;
            font-size: 12px;
        }
        .panel
        {
            background-color: #FFFFFF;
            -moz-user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            margin: 12px 12px;
            padding: 6px 12px;
        }
        .btn
        {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0px;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            -moz-user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .btn-50
        {
            width: 50px;
        }
        .btn-100
        {
            width: 100px;
        }
        .btn-150
        {
            width: 150px;
        }
        .btn-200
        {
            width: 205px;
        }
        .btn-primary
        {
            color: #FFF;
            background-color: #428BCA;
            border-color: #357EBD;
        }
        .btn-primary:hover
        {
            color: #FFF;
            background-color: #357EBD;
            border-color: #428BCA;
        }
        .form-control
        {
            display: block;
            width: 100%;
            height: 24px;
            padding: 3px 6px;
            font-size: 12px; /*line-height: 1.42857;*/
            color: #555;
            background-color: #FFF;
            background-image: none;
            border: 1px solid #bdbdbd;
            border-radius: 4px;
            box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset;
            transition: border-color 0.15s ease-in-out 0s, box-shadow 0.15s ease-in-out 0s;
        }
        textarea.form-control
        {
            height: auto;
        }
        .text-bold
        {
            font-weight: bold;
        }
        .img
        {
            min-width: 125px;
            min-height: 155px;
            width: 125px;
            height: 155px;
            border: 1px solid #CCC;
            border-radius: 4px;
            box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset;
            background-color: #FFFFFF;
        }
    </style>

    <script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>

</head>
<body>
    <div>
        <table style="100%">
            <tr>
                <td width="20%">
                    <table style="border: 1;">
                        <tr>
                            <td>
                              <form method="post" action="https://preprod.aadhaarbridge.com/kua/_init">
                                  <input type="hidden" name="saCode" value="16d390" >
                                  <input type="text" name="aadhaarId" id="aadhaarId" value="<?= $aadhaarId; ?>" >
                                  <input type="hidden" name="requestId" value="<?= $requestId; ?>" >
                                  <input type="hidden" name="purpose" value="TEsting by vJ" >
                                  <input type="hidden" name="modality" value="biometric" >
                                  <!-- <input type="hidden" name="channel" value="BOTH" > -->
                                  <input type="hidden" name="successUrl" value="http://192.168.1.112/aadhaar/success.php" >
                                  <input type="hidden" name="failureUrl" value="http://192.168.1.112/aadhaar/failure.php"  >
                                  <input type="hidden" name="hash" value="<?= $hash; ?>" >
                                  <input type="hidden" name="auth-capture-data" id="auth_capture_data" value='' >

                                  <button type="submit">Proceed to KYC</button>
                              </form >
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-primary btn-200" onclick="discoverAvdm();" type="button" value="Discover AVDM">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input class="btn btn-primary btn-200" onclick="deviceInfoAvdm();" type="button"
                                    value="Device Info">
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="55%">
                    <table style="border: 1;">
                        <tr>
                            <td>
                                Select Option to Capture
                            </td>
                        </tr>
                        <tr>
                            <td>
                                AVDM
                            </td>
                            <td colspan="6px">
                                <select id="ddlAVDM" class="form-control" style="width: 100%;">
                                    <option></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Timeout
                            </td>
                            <td>
                                <select id="Timeout" class="form-control">
                                    <option>10000</option>
                                    <option>10000</option>
                                    <option>20000</option>
                                    <option>30000</option>
                                    <option>40000</option>
                                    <option>50000</option>
                                    <option>60000</option>
                                    <option>70000</option>
                                    <option>80000</option>
                                    <option>90000</option>
                                    <option>100000</option>
                                    <option>0</option>
                                </select>
                            </td>
                            <td>
                                PidVer
                            </td>
                            <td width="60px">
                                <select id="Pidver" class="form-control">
                                    <option>2.0</option>
                                </select>
                            </td>
                            <td>
                                Env
                            </td>
                            <td width="60px">
                                <select id="Env" class="form-control">
                                    <option>S</option>
                                    <option selected="true">PP</option>
                                    <option>P</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Wadh
                            </td>
                            <td colspan="4px">
                                <textarea id="txtWadh" style="width: 100%; height: 50px;" class="form-control"><?= "rhVuL7SnJi2W2UmsyukVqY7c93JWyL9O/kVKgdNMfv8="; ?></textarea>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                DataType
                            </td>
                            <td colspan="4px">
                                <select id="Dtype" style="width: 45px;" class="form-control">
                                    <option>X</option>
                                    <option>P</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="40%">
                    <table style="border: 1;">
                        <tr>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Finger Count
                            </td>
                            <td>
                                <select id="Fcount" class="form-control">
                                    <option selected="selected">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                    <option>7</option>
                                    <option>8</option>
                                    <option>9</option>
                                    <option>10</option>
                                </select>
                            </td>
                            <td>
                                Finger Type
                            </td>
                            <td>
                                <select id="Ftype" class="form-control">
                                    <option value="0">FMR</option>
                                    <option value="1">FIR</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Iris Count
                            </td>
                            <td>
                                <select id="Icount" class="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </td>
                            <td>
                                Iris Type
                            </td>
                            <td>
                                <select id="Itype" class="form-control">
                                    <option>SELECT</option>
                                    <option>ISO</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Face Count
                            </td>
                            <td>
                                <select id="Pcount" class="form-control">
                                    <option>0</option>
                                    <option>1</option>
                                </select>
                            </td>
                            <td>
                                Face Type
                            </td>
                            <td>
                                <select id="Ptype" class="form-control">
                                    <option>SELECT</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </br>
        <table>
            <tbody>
                <tr>
                    <td>
                        <strong>PERSONAL IDENTITY(PI)</strong><br />
                        <table border="1" width="600px">
                            <tbody>
                                <tr>
                                    <td style="text-align: right;">
                                        Name:
                                    </td>
                                    <td>
                                        <input id="txtName" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Match Value:
                                    </td>
                                    <td>
                                        <select id="drpMatchValuePI" class="form-control">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        Match Strategy:
                                    </td>
                                    <td>
                                        <input name="RDPI" id="rdExactPI" checked="true" type="radio">Exact</input>
                                        <input name="RDPI" id="rdPartialPI" type="radio">Partial</input>
                                        <input name="RDPI" id="rdFuzzyPI" type="radio">Fuzzy</input>
                                    </td>
                                    <td style="text-align: right;">
                                        Age:
                                    </td>
                                    <td>
                                        <input id="txtAge" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        Local Name:
                                    </td>
                                    <td>
                                        <input id="txtLocalNamePI" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        LocalMatchValue:
                                    </td>
                                    <td>
                                        <select id="drpLocalMatchValuePI">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        DOB:
                                    </td>
                                    <td>
                                        <input id="txtDOB" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Gender:
                                    </td>
                                    <td>
                                        <select id="drpGender" class="form-control">
                                            <option value="0">Select</option>
                                            <option>MALE</option>
                                            <option>FEMALE</option>
                                            <option>TRANSGENDER</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        Phone:
                                    </td>
                                    <td>
                                        <input id="txtPhone" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Email:
                                    </td>
                                    <td>
                                        <input id="txtEmail" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        DOB Type:
                                    </td>
                                    <td>
                                        <select id="drpDOBType" class="form-control">
                                            <option value="0">select</option>
                                            <option>V</option>
                                            <option>D</option>
                                            <option>A</option>
                                        </select>
                                    </td>
                                    <td style="text-align: right;">
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td>
                        <strong>PERSONAL ADDRESS(PA)</strong><br />
                        <table border="1" width="600px">
                            <tbody>
                                <tr>
                                    <td style="text-align: right;">
                                        Care Of:
                                    </td>
                                    <td>
                                        <input id="txtCareOf" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Building:
                                    </td>
                                    <td>
                                        <input id="txtBuilding" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        Landmark:
                                    </td>
                                    <td>
                                        <input id="txtLandMark" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Street:
                                    </td>
                                    <td>
                                        <input id="txtStreet" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        Locality:
                                    </td>
                                    <td>
                                        <input id="txtLocality" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        PO Name:
                                    </td>
                                    <td>
                                        <input id="txtPOName" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        City:
                                    </td>
                                    <td>
                                        <input id="txtCity" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Sub Dist:
                                    </td>
                                    <td>
                                        <input id="txtSubDist" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        District:
                                    </td>
                                    <td>
                                        <input id="txtDist" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        State:
                                    </td>
                                    <td>
                                        <input id="txtState" type="text" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;">
                                        PinCode:
                                    </td>
                                    <td>
                                        <input id="txtPinCode" type="text" />
                                    </td>
                                    <td style="text-align: right;">
                                        Match Strategy:
                                    </td>
                                    <td>
                                        <input id="rdMatchStrategyPA" checked="true" type="radio">Exact</input>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        </br>
        <table border="1" width="1200px">
            <tbody>
                <tr>
                    <td colspan="6">
                        <strong>PERSONAL FULL ADDRESS(PFA)</strong>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        Match Strategy:
                    </td>
                    <td>
                        <input name="RD" id="rdExactPFA" checked="true" type="radio">Exact</input>
                        <input name="RD" id="rdPartialPFA" type="radio">Partial </input>
                        <input name="RD" id="rdFuzzyPFA" type="radio">Fuzzy</input>
                    </td>
                    <td style="text-align: right;">
                        Match Value:
                    </td>
                    <td>
                        <select id="drpMatchValuePFA">
                        </select>
                    </td>
                    <td style="text-align: right;">
                        Local Match Value:
                    </td>
                    <td>
                        <select id="drpLocalMatchValue">
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: right;">
                        Address Value:
                    </td>
                    <td colspan="3">
                        <textarea id="txtAddressValue" style="width: 100%; height: 50px;" class="form-control"></textarea>
                    </td>
                    <td style="text-align: right;">
                        Local Address:
                    </td>
                    <td colspan="3">
                        <textarea id="txtLocalAddress" style="width: 100%; height: 50px;" class="form-control"></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="border: 1; width: 20%">
            <tr>
                <td>
                    <input type="button" class="btn btn-primary btn-200" onclick="CaptureAvdm();" value="Capture" />
                </td>
                <td>
                    <input type="button" class="btn btn-primary btn-200" onclick="reset();" value="Reset" />
                </td>
                <!-- <td><input type="button" class="btn btn-primary btn-200" onclick="Demo();" value="Auth"/></td> -->
            </tr>
        </table>
        <label>
            avdm/device info</label>
        <textarea id="txtDeviceInfo" style="width: 100%; height: 160px;" class="form-control"> </textarea>
        <label>
            pid options</label>
        <textarea id="txtPidOptions" style="width: 100%; height: 100px;" class="form-control"> </textarea>
        <label>
            pid data</label>
        <textarea id="txtPidData" style="width: 100%; height: 150px;" class="form-control"> </textarea>
        <label id="lblstatus">
        </label>

        <script language="javascript" type="text/javascript">


		<!-- $(document).ready(function (){ -->
			<!-- alert("D m calling..."); -->
		<!-- }); -->
		var GetPIString='';
		var GetPAString='';
		var GetPFAString='';
		var DemoFinalString='';
		var select = '';
		select += '<option val=0>Select</option>';
		for (i=1;i<=100;i++){
			select += '<option val=' + i + '>' + i + '</option>';
		}
		$('#drpMatchValuePI').html(select);
		$('#drpMatchValuePFA').html(select);
		$('#drpLocalMatchValue').html(select);
		$('#drpLocalMatchValuePI').html(select);

		var finalUrl="";
		var MethodInfo="";
		var MethodCapture="";

		function test()
		{
			alert("I am calling..");
		}

		function reset()
		{
			$('#txtWadh').val('');
		    $('#txtDeviceInfo').val('');
			$('#txtPidOptions').val('');
			$('#txtPidData').val('');
		    $("select#ddlAVDM").prop('selectedIndex', 0);
		    $("select#Timeout").prop('selectedIndex', 0);
			$("select#Icount").prop('selectedIndex', 0);
			$("select#Fcount").prop('selectedIndex', 0);
			$("select#Icount").prop('selectedIndex', 0);
			$("select#Itype").prop('selectedIndex', 0);
			$("select#Ptype").prop('selectedIndex', 0);
			$("select#Ftype").prop('selectedIndex', 0);
			$("select#Dtype").prop('selectedIndex', 0);
		}
		// All New Function

		function Demo()
		{

		var GetPIStringstr='';
		var GetPAStringstr='';
		var GetPFAStringstr='';

			if(GetPI()==true)
			{
				GetPIStringstr ='<Pi '+GetPIString+' />';
				//alert(GetPIStringstr);
			}
			else
			{
				GetPIString='';
			}

			if(GetPA()==true)
			{
				GetPAStringstr ='<Pa '+GetPAString+' />';
				//alert(GetPAStringstr);
			}
			else
			{
				GetPAString='';
			}

			if(GetPFA()==true)
			{
				GetPFAStringstr ='<Pfa '+GetPFAString+' />';
				//alert(GetPFAStringstr);
			}
			else
			{
				GetPFAString='';
			}

			if(GetPI()==false && GetPA()==false && GetPFA()==false)
			{
				//alert("Fill Data!");
				DemoFinalString='';
			}
			else
			{
				DemoFinalString = '<Demo>'+ GetPIStringstr +' ' + GetPAStringstr + ' ' + GetPFAStringstr + ' </Demo>';
				//alert(DemoFinalString)
			}
		}

		function GetPI()
		{
			var Flag=false;
			GetPIString='';

			 if ($("#txtName").val().trim().length > 0)
            {
                Flag = true;
                GetPIString += "name="+ "\""+$("#txtName").val().trim()+"\"";
            }

            if ($("#drpMatchValuePI").val() > 0 && Flag)
            {
                Flag = true;
				GetPIString += " mv="+ "\""+$("#drpMatchValuePI").val().trim()+"\"";
            }

			if ($('#rdExactPI').is(':checked') && Flag)
            {
                Flag = true;
                GetPIString += " ms="+ "\"E\"";
            }
            else if ($('#rdPartialPI').is(':checked') && Flag)
            {
                Flag = true;
               GetPIString += " ms="+ "\"P\"";
            }
            else if ($('#rdFuzzyPI').is(':checked') && Flag)
            {
                Flag = true;
                GetPIString += " ms="+ "\"F\"";
            }
			if ($("#txtLocalNamePI").val().trim().length > 0)
            {
				Flag = true;
                GetPIString += " lname="+ "\""+$("#txtLocalNamePI").val().trim()+"\"";
            }

			if ($("#txtLocalNamePI").val().trim().length > 0 && $("#drpLocalMatchValuePI").val() > 0)
            {
				Flag = true;
				GetPIString += " lmv="+ "\""+$("#drpLocalMatchValuePI").val().trim()+"\"";
            }

            <!-- if ($("#drpGender").val() > 0) -->
            <!-- { -->

                if ($("#drpGender").val().trim() == "MALE")
                {
                    Flag = true;
					 GetPIString += " gender="+ "\"M\"";
                }
                else if ($("#drpGender").val().trim() == "FEMALE")
                {
                    Flag = true;
                     GetPIString += " gender="+ "\"F\"";
                }
                else if ($("#drpGender").val().trim() == "TRANSGENDER")
                {
                    Flag = true;
                   GetPIString += " gender="+ "\"T\"";
                }
            //}
			    if ($("#txtDOB").val().trim().length > 0 )
				{
					Flag = true;
					GetPIString += " dob="+ "\""+$("#txtDOB").val().trim()+"\"";
				}

				if ($("#drpDOBType").val() != "0")
				{
					Flag = true;
					GetPIString += " dobt="+ "\""+$("#drpDOBType").val().trim()+"\"";
				}

				if ($("#txtAge").val().trim().length)
				{
					Flag = true;
					GetPIString += " age="+ "\""+$("#txtAge").val().trim()+"\"";
				}

				if ($("#txtPhone").val().trim().length > 0 || $("#txtEmail").val().trim().length > 0)
				{
					Flag = true;
					GetPIString += " phone="+ "\""+$("#txtPhone").val().trim()+"\"";
				}
				if ($("#txtEmail").val().trim().length > 0)
				{
					Flag = true;
					GetPIString += " email="+ "\""+$("#txtEmail").val().trim()+"\"";
				}

			//alert(GetPIString);
			return Flag;
		}


		function GetPA()
		{
			var Flag=false;
			GetPAString='';

			if ($("#txtCareOf").val().trim().length > 0)
            {
				Flag = true;
                GetPAString += "co="+ "\""+$("#txtCareOf").val().trim()+"\"";
            }
            if ($("#txtLandMark").val().trim().length > 0 )
            {
                Flag = true;
                GetPAString += " lm="+ "\""+$("#txtLandMark").val().trim()+"\"";
            }
            if ($("#txtLocality").val().trim().length > 0 )
            {
               Flag = true;
                GetPAString += " loc="+ "\""+$("#txtLocality").val().trim()+"\"";
            }
            if ($("#txtCity").val().trim().length > 0 )
            {
                Flag = true;
                GetPAString += " vtc="+ "\""+$("#txtCity").val().trim()+"\"";
            }
            if ($("#txtDist").val().trim().length > 0 )
            {
                Flag = true;
                GetPAString += " dist="+ "\""+$("#txtDist").val().trim()+"\"";
            }
            if ($("#txtPinCode").val().trim().length > 0 )
            {
                Flag = true;
                GetPAString += " pc="+ "\""+$("#txtPinCode").val().trim()+"\"";
            }
            if ($("#txtBuilding").val().trim().length > 0 )
            {
                 Flag = true;
                GetPAString += " house="+ "\""+$("#txtBuilding").val().trim()+"\"";
            }
            if ($("#txtStreet").val().trim().length > 0 )
            {
                 Flag = true;
                GetPAString += " street="+ "\""+$("#txtStreet").val().trim()+"\"";
            }
            if ($("#txtPOName").val().trim().length > 0 )
            {
                 Flag = true;
                GetPAString += " po="+ "\""+$("#txtPOName").val().trim()+"\"";
            }
            if ($("#txtSubDist").val().trim().length > 0 )
            {
                  Flag = true;
                GetPAString += " subdist="+ "\""+$("#txtSubDist").val().trim()+"\"";
            }
            if ($("#txtState").val().trim().length > 0)
            {
                 Flag = true;
                GetPAString += " state="+ "\""+$("#txtState").val().trim()+"\"";
            }
            if ( $('#rdMatchStrategyPA').is(':checked') && Flag)
            {
                Flag = true;
                GetPAString += " ms="+ "\"E\"";
            }
			//alert(GetPIString);
			return Flag;
		}



		function GetPFA()
		{
			var Flag=false;
			GetPFAString='';

			if ($("#txtAddressValue").val().trim().length > 0)
            {
				Flag = true;
                GetPFAString += "av="+ "\""+$("#txtAddressValue").val().trim()+"\"";
            }

			if ($("#drpMatchValuePFA").val() > 0 && $("#txtAddressValue").val().trim().length > 0)
            {
                Flag = true;
				GetPFAString += " mv="+ "\""+$("#drpMatchValuePFA").val().trim()+"\"";
            }

			if ($('#rdExactPFA').is(':checked') && Flag)
            {
                Flag = true;
                GetPFAString += " ms="+ "\"E\"";
            }
            else if ($('#rdPartialPFA').is(':checked') && Flag)
            {
                Flag = true;
               GetPFAString += " ms="+ "\"P\"";
            }
            else if ($('#rdFuzzyPFA').is(':checked') && Flag)
            {
                Flag = true;
                GetPFAString += " ms="+ "\"F\"";
            }

			if ($("#txtLocalAddress").val().trim().length > 0)
            {
				Flag = true;
                GetPFAString += " lav="+ "\""+$("#txtLocalAddress").val().trim()+"\"";
            }

			if ($("#drpLocalMatchValue").val() > 0 && $("#txtLocalAddress").val().trim().length > 0)
            {
                Flag = true;
				GetPFAString += " lmv="+ "\""+$("#drpLocalMatchValue").val().trim()+"\"";
            }
			//alert(GetPIString);
			return Flag;
		}

		$( "#ddlAVDM" ).change(function() {
		//alert($("#ddlAVDM").val());
		discoverAvdmFirstNode($("#ddlAVDM").val());
	    });

		function discoverAvdmFirstNode(PortNo)
		{

			$('#txtWadh').val('');
		    $('#txtDeviceInfo').val('');
			$('#txtPidOptions').val('');
			$('#txtPidData').val('');

		//alert(PortNo);

		var primaryUrl = "http://127.0.0.1:";
            url = "";
					 var verb = "RDSERVICE";
                        var err = "";
						var res;
						$.support.cors = true;
						var httpStaus = false;
						var jsonstr="";
						 var data = new Object();
						 var obj = new Object();

							$.ajax({
							type: "RDSERVICE",
							async: false,
							crossDomain: true,
							url: primaryUrl + PortNo,
							contentType: "text/xml; charset=utf-8",
							processData: false,
							cache: false,
							async:false,
							crossDomain:true,
							success: function (data) {
								httpStaus = true;
								res = { httpStaus: httpStaus, data: data };
							    //alert(data);

								//debugger;

								 $("#txtDeviceInfo").val(data);

								var $doc = $.parseXML(data);

								//alert($($doc).find('Interface').eq(1).attr('path'));

								MethodInfo=$($doc).find('Interface').eq(0).attr('path');
								MethodCapture=$($doc).find('Interface').eq(1).attr('path');

								 alert("RDSERVICE Discover Successfully");
							},
							error: function (jqXHR, ajaxOptions, thrownError) {
							$('#txtDeviceInfo').val("");
							alert(thrownError);
								res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
							},
						});

						return res;
		}

		function discoverAvdm()
		{
            <!-- ddlAVDM.empty(); -->

            var primaryUrl = "http://127.0.0.1:";
            url = "";
			 $("#ddlAVDM").empty();
			alert("Please wait while discovering port from 11100 to 11120.\nThis will take some time.");

                for (var i = 11100; i <= 11120; i++)
                {

                    $("#lblStatus").text("Discovering RD service on port : " + i.toString());

						var verb = "RDSERVICE";
                        var err = "";

						var res;
						$.support.cors = true;
						var httpStaus = false;
						var jsonstr="";
						 var data = new Object();
						 var obj = new Object();

							$.ajax({

							type: "RDSERVICE",
							async: false,
							crossDomain: true,
							url: primaryUrl + i.toString(),
							contentType: "text/xml; charset=utf-8",
							processData: false,
							cache: false,
							async:false,
							crossDomain:true,

							success: function (data) {

								httpStaus = true;
								res = { httpStaus: httpStaus, data: data };
							    //alert(data);
								 finalUrl = primaryUrl + i.toString();
								var $doc = $.parseXML(data);
								var CmbData1 =  $($doc).find('RDService').attr('status');
								var CmbData2 =  $($doc).find('RDService').attr('info');

								$("#ddlAVDM").append('<option value='+i.toString()+'>(' + CmbData1 +')'+CmbData2+'</option>')

							},
							error: function (jqXHR, ajaxOptions, thrownError) {
							//alert(thrownError);
								res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
							},
						});
						//$("#ddlAVDM").val("0");

                }
				$("select#ddlAVDM").prop('selectedIndex', 0);

				//$('#txtDeviceInfo').val(DataXML);

				var PortVal= $('#ddlAVDM').val($('#ddlAVDM').find('option').first().val()).val();

				if(PortVal>11099)
				{
				   discoverAvdmFirstNode(PortVal);
				}
				return res;
		}


		function deviceInfoAvdm()
		{
			//alert($("#ddlAVDM").val());
            <!-- ddlAVDM.empty(); -->


            url = "";

					<!-- alert(i.toString()); -->
                    // $("#lblStatus").text("Discovering RD Service on Port : " + i.toString());
					finalUrl="http://127.0.0.1:"+$("#ddlAVDM").val();
					 var verb = "DEVICEINFO";
                      //alert(finalUrl);

                        var err = "";

						var res;
						$.support.cors = true;
						var httpStaus = false;
						var jsonstr="";
						;
							$.ajax({

							type: "DEVICEINFO",
							async: false,
							crossDomain: true,
							url: finalUrl+MethodInfo,
							contentType: "text/xml; charset=utf-8",
							processData: false,
							success: function (data) {
							//alert(data);
								httpStaus = true;
								res = { httpStaus: httpStaus, data: data };

								$('#txtDeviceInfo').val(data);
							},
							error: function (jqXHR, ajaxOptions, thrownError) {
							alert(thrownError);
								res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
							},
						});

						return res;

		}



		function CaptureAvdm()
		{

	   Demo();
	   if($("#txtWadh").val().trim()!="")
	   {
		var XML='<?xml version="1.0"?> <PidOptions ver="1.0"> <Opts fCount="'+$("#Fcount").val()+'" fType="'+$("#Ftype").val()+'" iCount="'+$("#Icount").val()+'" pCount="'+$("#Pcount").val()+'" format="0" pidVer="'+$("#Pidver").val()+'" timeout="'+$("#Timeout").val()+'" wadh="'+$("#txtWadh").val()+'" posh="UNKNOWN" env="'+$("#Env").val()+'" /> '+DemoFinalString+' </PidOptions>';
	   }
	   else
	   {
		var XML='<?xml version="1.0"?> <PidOptions ver="1.0"> <Opts fCount="'+$("#Fcount").val()+'" fType="'+$("#Ftype").val()+'" iCount="'+$("#Icount").val()+'" pCount="'+$("#Pcount").val()+'" format="0" pidVer="'+$("#Pidver").val()+'" timeout="'+$("#Timeout").val()+'" posh="UNKNOWN" env="'+$("#Env").val()+'" /> '+DemoFinalString+' </PidOptions>';
	   }
			//alert(XML);

            <!-- url = ""; -->

					 var verb = "CAPTURE";


                        var err = "";

						var res;
            var json = '';
						$.support.cors = true;
						var httpStaus = false;
						var jsonstr="";
						;
							$.ajax({

							type: "CAPTURE",
							async: false,
							crossDomain: true,
							url: finalUrl+MethodCapture,
							data:XML,
							contentType: "text/xml; charset=utf-8",
							processData: false,
							success: function (data) {
							//alert(data);
								httpStaus = true;
								res = { httpStaus: httpStaus, data: data };

								$('#txtPidData').val(data);
								$('#txtPidOptions').val(XML);
                // console.log($(data).find('mc').text());


								var $doc = $.parseXML(data);
								var Message =  $($doc).find('Resp').attr('errInfo');
								var pid_type =  $($doc).find('Data').attr('type');
								var pid_value =  $($doc).find('Data').text();
								var Hmac =  $($doc).find('Hmac').text();
								var cert_id =  $($doc).find('Skey').attr('ci');
								var cert_value =  $($doc).find('Skey').text();
								var unique_device_code =  $($doc).find('DeviceInfo').find('additional_info').find('Param').attr('value');
                var dpId =  $($doc).find('DeviceInfo').attr('dpId');
                var rdsId =  $($doc).find('DeviceInfo').attr('rdsId');
                var rdsVer =  $($doc).find('DeviceInfo').attr('rdsVer');
                var dc =  $($doc).find('DeviceInfo').attr('dc');
                var mi =  $($doc).find('DeviceInfo').attr('mi');
                var mc =  $($doc).find('DeviceInfo').attr('mc');
                var aadhaarId =  $('#aadhaarId').val();

                item = {};

                item['consent'] = 'Y';
                item['modality'] = {};
                    item['modality']['demographics'] = false;
                    item['modality']['fp-image'] = 'false';
                    item['modality']['fp-minutae'] = true;
                    item['modality']['iris'] = 'false';
                    item['modality']['otp'] = 'false';
                    item['modality']['pin'] = 'false';
                item['pid'] = {};
                    item['pid']['type'] = 'xml';
                    item['pid']['value'] = pid_value;
                item['aadhaar-id'] = aadhaarId;
                item['hmac'] = Hmac;
                item['session-key'] = {};
                    item['session-key']['cert-id'] = cert_id;
                    item['session-key']['value'] = cert_value;
                item['unique-device-code'] = unique_device_code;
                item['dpId'] = dpId;
                item['rdsId'] = rdsId;
                item['rdsVer'] = rdsVer;
                item['dc'] = dc;
                item['mi'] = mi;
                item['mc'] = mc;
                // var new_json = [
                //   'consent'=> 'Y',
                //   'modality' => [
                //     'demographics' => false,
                //     'fp-image' => false,
                //     'fp-minutae' => false,
                //     'iris' => false,
                //     'otp' => true,
                //     'pin' => false,
                //   ],
                //   'pid' => [
                //     'type' => pid_type,
                //     'value' => pid_value,
                //   ],
                //   'aadhaar-id' => aadhaarId,
                //   'hmac' => Hmac],
                //   'session-key' => [
                //     'cert-id' => cert_id,
                //     'value' => cert_value,
                //   ],
                //   'unique-device-code' => unique_device_code,
                //   'dpId' => dpId,
                //   'rdsId' => rdsId,
                //   'rdsVer' => rdsVer,
                //   'dc' => dc,
                //   'mi' => mi,
                //   'mc' => mc
                // ];
                // json.push(item);
                $('#auth_capture_data').val(JSON.stringify(item));

                // $('#pid_type').val(pid_type);
                // $('#pid_value').val(pid_value);
                // $('#Hmac').val(Hmac);
                // $('#cert_id').val(cert_id);
                // $('#cert_value').val(cert_value);
                // $('#unique_device_code').val(unique_device_code);
                // $('#dpId').val(dpId);
                // $('#rdsId').val(rdsId);
                // $('#rdsVer').val(rdsVer);
                // $('#dc').val(dc);
                // $('#mi').val(mi);
                // $('#mc').val(mc);

								alert(Message);
                console.log(JSON.stringify(item));

							},
							error: function (jqXHR, ajaxOptions, thrownError) {
							//$('#txtPidOptions').val(XML);
							alert(thrownError);
								res = { httpStaus: httpStaus, err: getHttpError(jqXHR) };
							},
						});

						return res;
		}
		function getHttpError(jqXHR) {
		    var err = "Unhandled Exception";
		    if (jqXHR.status === 0) {
		        err = 'Service Unavailable';
		    } else if (jqXHR.status == 404) {
		        err = 'Requested page not found';
		    } else if (jqXHR.status == 500) {
		        err = 'Internal Server Error';
		    } else if (thrownError === 'parsererror') {
		        err = 'Requested JSON parse failed';
		    } else if (thrownError === 'timeout') {
		        err = 'Time out error';
		    } else if (thrownError === 'abort') {
		        err = 'Ajax request aborted';
		    } else {
		        err = 'Unhandled Error';
		    }
		    return err;
		}
        </script>
</body>
</html>
