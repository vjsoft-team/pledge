<?php

$saCode = '16d390';
$aadhaarId = '515383281069';
$requestId = "611689862";
$uuid = "3c0406e9-206a-453c-9b35-d410cf823914";
$salt = 'c8b6a27e69';
$hash_seq = $uuid . '|' . $saCode . '|' . $aadhaarId . '|' . $requestId . '|' . $salt;
$hash = hash( 'sha256', $hash_seq );

$data = [
  "saCode" => $saCode,
  "uuid" => $uuid,
  "requestId" => $requestId,
  "aadhaarId" => $aadhaarId,
  "hash" => $hash
];
$data_string = json_encode($data);

$ch = curl_init('https://preprod.aadhaarbridge.com/kua/_kyc');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
  'Content-Type: application/json',
  'Content-Length: ' . strlen($data_string))
);

$result = curl_exec($ch);

print_r($result);
?>
