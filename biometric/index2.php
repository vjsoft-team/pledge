<?php

  $saCode = '16d390';
  $aadhaarId = '286004871682';
  $requestId = '45';
  $salt = 'c8b6a27e69';
  $hash_seq = $saCode . '|' . $aadhaarId . '|' . $requestId . '|' . $salt;
  $hash = hash( 'sha256', $hash_seq );


  $json = [];

  $json[] = [
    'consent'=> 'Y'
  ];

  echo json_encode($json);

?>
<form method="post" action="https://preprod.aadhaarbridge.com/kua/_init">
    <input type="hidden" name="saCode" value="16d390" >
    <input type="text" name="aadhaarId" value="286004871682" >
    <input type="hidden" name="requestId" value="<?= $requestId; ?>" >
    <input type="hidden" name="purpose" value="purpose of doing e kyc" >
    <input type="hidden" name="modality" value="biometric" >
    <input type="hidden" name="channel" value="BOTH" >
    <input type="hidden" name="successUrl" value="http://192.168.1.112/aadhaar/success.php" >
    <input type="hidden" name="failureUrl" value="http://192.168.1.112/aadhaar/failure.php"  >
    <input type="hidden" name="hash" value="<?= $hash; ?>" >
    <input type="hidden" name="auth-capture-data" value="<?= $bio; ?>" >
    <button type="submit">Proceed to KYC</button>
</form >
